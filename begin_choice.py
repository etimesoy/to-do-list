import time
import os
from classes import Task
from functions import print_list, try_except, except_text, list_choice

# сообщения, которые будут появляться при некорректном вводе номера задачи
text1, text2 = except_text('задача')


def choice_1(list_of_task_lists, cur_list):
    list_of_task_lists[cur_list].tasks.append(Task())
    list_of_task_lists[cur_list].tasks[-1].new()
    print()


def choice_2(list_of_task_lists, cur_list):
    if len(list_of_task_lists[cur_list].tasks) == 0:
        print('Задач нет, отдыхайте!\n')
    else:
        print_list(list_of_task_lists[cur_list].tasks)
        d = len(list_of_task_lists[cur_list].tasks)
        num_of_task = input('Какую задачу вы хотите изменить? Введите ее номер: ')
        num_of_task = try_except(num_of_task, d, text1, text2)
        print()
        list_of_task_lists[cur_list].tasks[num_of_task].show()
        print()
        list_of_task_lists[cur_list].tasks[num_of_task].change()
        print()


def choice_3(list_of_task_lists, cur_list, user_name):
    if len(list_of_task_lists[cur_list].tasks) == 0:
        print('Задач нет, отдыхайте!\n')
    else:
        print_list(list_of_task_lists[cur_list].tasks)
        d = len(list_of_task_lists[cur_list].tasks)
        num_of_task = input('Какая задача выполнена? Введите ее номер: ')
        num_of_task = try_except(num_of_task, d, text1, text2)
        temp = list_of_task_lists[cur_list].tasks[num_of_task]
        list_of_task_lists[cur_list].done_tasks.append(temp)
        del list_of_task_lists[cur_list].tasks[num_of_task]
        print(f'\nХорошая работа, {user_name}!')
        time.sleep(2)
        os.system("cls")


def choice_4(list_of_task_lists, cur_list):
    if len(list_of_task_lists[cur_list].tasks) == 0:
        print('Задач нет, отдыхайте!\n')
    else:
        print_list(list_of_task_lists[cur_list].tasks)
        d = len(list_of_task_lists[cur_list].tasks)
        num_of_task = input('Какую задачу удалить? Введите ее номер: ')
        num_of_task = try_except(num_of_task, d, text1, text2)
        print('\nЗадача №', num_of_task + 1, 'удалена', sep=' ')
        del list_of_task_lists[cur_list].tasks[num_of_task]
        time.sleep(1.5)
        os.system("cls")


def choice_5(list_of_task_lists, cur_list):
    if len(list_of_task_lists[cur_list].tasks) == 0:
        print('Задач нет, отдыхайте!\n')
    else:
        print_list(list_of_task_lists[cur_list].tasks)


def choice_6(list_of_task_lists, cur_list):
    if len(list_of_task_lists[cur_list].done_tasks) == 0:
        print('Пока что нет выполненных задач\n')
    else:
        print_list(list_of_task_lists[cur_list].done_tasks, 'выполненных ')


def choice_8(list_of_task_lists, count_of_lists, cur_list):
    print_list(list_of_task_lists, 'списков ')
    return list_choice(list_of_task_lists, count_of_lists, cur_list)


def choice_9(list_of_task_lists, cur_list):
    if len(list_of_task_lists[cur_list].tasks) == 0:
        print('Задач нет, отдыхайте!\n')
    else:
        print_list(list_of_task_lists[cur_list].tasks)
        d = len(list_of_task_lists[cur_list].tasks)
        num_of_task = input('Какую задачу вы хотите посмотреть? Введите ее номер: ')
        num_of_task = try_except(num_of_task, d, text1, text2)
        print()
        list_of_task_lists[cur_list].tasks[num_of_task].show()
        print()
