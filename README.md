# Console TO-DO list
**About**
-----
This repository includes simple console TO-DO list with basic functionality.
All user information is saved in `*user_name*.txt` file in the root of repository. 

**Setup**
-----
**Installation**
###
All you need to do is clone repository using this command:

    git clone https://gitlab.com/etimesoy/to-do-list.git

**Launching**
###
To run the program use this command in the root of the repository:

    python main.py

**Characteristics**
-----
You can divide tasks into different lists (ex. work, study, personal).

**Every task list has:**
* Name
* Tasks
* Complete tasks

**And every task has:**
* Name
* Expiration date
* Reminder
* Priority
* Description

**Functionality**
-----
**Commands using for task lists:**
1. Add
2. Rename
3. Delete
4. Show tasks
5. Show complete tasks
6. Change current list
7. Go back to current list (it is always shown)
8. Show all

**Commands using for tasks:**
1. Add
2. Change
3. Mark as completed
4. Delete
5. Show information

**Navigation**
-----
File name         |     File content
------------------|----------------------
Sequence of values in user file.txt | The order of information in the user file 
begin_choice.py   | Functions that are used to process the user task command
classes.py        | List and Task classes
file_functions.py | Functions working with files
functions.py      | All functions
main.py           | Start point of program, work with files

**About me**
-----
I am a freshman in "Высшая школа ИТИС" with more than 2 years of experience in programming.

**Contact**
-----
You can contact me using email *ruslangazizov36@gmail.com* for any questions.
