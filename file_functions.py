from classes import List


def write_data_into_file(f, password, count_of_lists, list_of_task_lists):
    """
    Writes information from variables into file

    :param f: file
    :param password: user's hashed password
    :param count_of_lists: number of user's task lists
    :param list_of_task_lists: all user's task lists are included in this list
    :return: file with updated data
    """
    f.write(password + '\n')
    f.write(str(count_of_lists) + '\n')
    for i in range(count_of_lists):
        temp_line = str(list_of_task_lists[i].name) + ' ' + str(len(list_of_task_lists[i].tasks))
        f.write(temp_line + '\n')
        for j in range(len(list_of_task_lists[i].tasks)):
            temp_task = list_of_task_lists[i].tasks[j].name + ', ' + \
                         list_of_task_lists[i].tasks[j].expiration_date + ', ' + \
                         list_of_task_lists[i].tasks[j].reminder + ', ' + \
                         str(list_of_task_lists[i].tasks[j].priority) + ', ' + \
                         list_of_task_lists[i].tasks[j].description
            f.write(temp_task + '\n')
        temp_line = str(len(list_of_task_lists[i].done_tasks))
        f.write(temp_line + '\n')
        for j in range(len(list_of_task_lists[i].done_tasks)):
            temp_done_task = list_of_task_lists[i].done_tasks[j].name + ', ' + \
                              list_of_task_lists[i].done_tasks[j].expiration_date + ', ' + \
                              list_of_task_lists[i].done_tasks[j].reminder + ', ' + \
                              str(list_of_task_lists[i].done_tasks[j].priority) + ', ' + \
                              list_of_task_lists[i].done_tasks[j].description
            f.write(temp_done_task + '\n')
    return f


def read_data_from_file(f):
    """
    Takes data from files and writes it in count_of_lists, list_of_task_lists

    :param f: file
    :return: number of user's task lists and information about all user's task lists
    """
    count_of_lists = int(f.readline().strip())  # количество списков задач
    list_of_task_lists = []
    for i in range(count_of_lists):
        list_of_task_lists.append(List('', [], []))
        task_list, done_task_list = [], []
        list_of_task_lists[i].name, count_of_tasks = f.readline().strip().split()
        count_of_tasks = int(count_of_tasks)
        for j in range(count_of_tasks):
            task_list.append(f.readline().strip())
        list_of_task_lists[i].read_tasks(task_list)
        count_of_done_tasks = int(f.readline().strip())
        for j in range(count_of_done_tasks):
            done_task_list.append(f.readline().strip())
        list_of_task_lists[i].read_done_tasks(done_task_list)
    return count_of_lists, list_of_task_lists
