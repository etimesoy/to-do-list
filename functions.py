import os
from classes import List


def except_text(value):
    """
    Creates messages that will appear if the task number is entered incorrectly

    :param value: 'список', 'задача', 'цифра'. Depends on what messages are needed
    :return: 2 messages that will appear if the task number is entered incorrectly
    """
    if value == 'список':
        return 'Пожалуйста, введите номер списка: ', 'Пожалуйста, выберите один список из представленных: '
    elif value == 'задача':
        return 'Пожалуйста, введите номер задачи: ', 'Пожалуйста, выберите одну задачу из представленных: '
    elif value == 'цифра':
        return 'Пожалуйста, введите цифру: ', 'Пожалуйста, введите цифру от 1 до 3: '


def try_except(var, d, text1, text2):
    """
    Helps to deal with exceptions

    :param var: string, that needs to be converted to integer
    :param d: right border of possible values for integer
    :param text1: message that will appear if the task number is not integer
    :param text2: message that will appear if the task number is out of range
    :return: integer value of var minus one
    """
    while True:
        try:
            var = int(var) - 1
        except ValueError:
            var = input(text1)
        else:
            if var < 0 or var >= d:
                var = input(text2)
            else:
                return var


def color(priority, text):
    """
    Colors the text according to the priority of the task

    :param priority: priority of task
    :param text: string, that needs to be colored
    :return: colored text
    """
    if priority == 1:  # красный
        return f'\033[31m{text}\033[0m'
    elif priority == 2:  # желтый
        return f'\033[33m{text}\033[0m'
    elif priority == 3:  # синий
        return f'\033[34m{text}\033[0m'


def print_list(lst, text=''):
    """
    Prints task list or list of task lists

    :param lst: list, that is needed to be printed
    :param text: 'списков ', if we deal with list of task lists
    :return: printed list in console with an empty line after
    """
    print(f'У вас есть {len(lst)} {text}задач:')
    try:
        lst[0].expiration_date
    except AttributeError:
        for i in range(len(lst)):
            print(i + 1, ') ', lst[i].name, sep='')
    else:
        for i in range(len(lst)):
            print(color(lst[i].priority, i + 1), ') ', lst[i].name, ' ', lst[i].expiration_date, sep='')
    finally:
        print()


def add_priority(temp):
    """
    Adds priority to task

    :param temp: string, that needs to be converted to integer
    :return: integer value of temp
    """
    if temp == '':  # по умолчанию приоритет низкий
        return 3
    text1, text2 = except_text('цифра')
    return try_except(temp, 3, text1, text2) + 1


def list_choice(list_of_task_lists, count_of_lists, cur_list):
    """
    This function is used to process the user task lists commands

    :param list_of_task_lists: all user's task lists are included in this list
    :param count_of_lists: number of user's task lists
    :param cur_list: number of the list we are currently on
    :return: necessary information to write back into file
    """
    while True:
        print('Текущий список: ', list_of_task_lists[cur_list].name, '. Доступные команды:', sep='')
        print('1) Добавить список', '2) Изменить название списка', '3) Удалить список', '4) Перейти к списку')
        print('5) Вернуться к текущему списку', '6) Показать списки задач')
        choice = input('\nЧто вы хотите сделать? Введите цифру: ')
        try:
            choice = int(choice)
        except ValueError:
            os.system("cls")
            print('Пожалуйста, введите число\n')
        else:
            text1, text2 = except_text('список')
            os.system("cls")
            if choice < 1 or choice > 6:
                print('Пожалуйста, выберите одну команду из представленных')
            elif choice == 1:
                name = input('Введите название нового списка задач: ')
                count_of_lists += 1
                list_of_task_lists.append(List(name, [], []))
                choice_ = input('Перейти к этому списку? (Да/Нет) ')
                yes_no = ['да', 'Да', 'ДА', 'нет', 'Нет', 'НЕТ']
                while choice_ not in yes_no:
                    choice_ = input('Пожалуйста, ответьте на этот вопрос: ')
                if choice_ in yes_no[:3]:
                    cur_list = count_of_lists - 1
                    print()
                    break
            elif choice == 2:
                print_list(list_of_task_lists, 'списков ')
                choice_ = input('Название какого именно список вы хотите изменить? ')
                choice_ = try_except(choice_, count_of_lists, text1, text2)
                list_of_task_lists[choice_].name = input('Введите новое название: ')
            elif choice == 3:
                print_list(list_of_task_lists, 'списков ')
                choice_ = input('Какой именно список вы хотите удалить? ')
                choice_ = try_except(choice_, count_of_lists, text1, text2)
                print('\nСписок "', list_of_task_lists[choice_].name, '" успешно удален', sep='')
                del list_of_task_lists[choice_]
                count_of_lists -= 1
                if cur_list == choice_:
                    cur_list = 0
                    if count_of_lists == 0:
                        print('Это был последний список задач')
                        print('\nСоздайте новый список задач!')
                        list_of_task_lists.append(List('', [], []))
                        list_of_task_lists[-1].name = input('Введите его название: ')
                        count_of_lists = 1
                elif cur_list > choice_:
                    cur_list -= 1
            elif choice == 4:
                print_list(list_of_task_lists, 'списков ')
                choice_ = input('К какому именно списку вы хотите перейти? ')
                cur_list = try_except(choice_, count_of_lists, text1, text2)
                os.system("cls")
                break
            elif choice == 5:
                break
            elif choice == 6:
                print_list(list_of_task_lists, 'списков ')
            if choice != 6:
                print()
    return list_of_task_lists, count_of_lists, cur_list


from begin_choice import choice_1, choice_2, choice_3, choice_4, choice_5, choice_6, choice_8, choice_9


def begin(list_of_task_lists, user_name, count_of_lists, cur_list):
    """
    This function is used to process the user task commands

    :param list_of_task_lists: all user's task lists are included in this list
    :param user_name: user's name
    :param count_of_lists: number of user's task lists
    :param cur_list: number of the list we are currently on
    :return: necessary information to write back into file
    """
    print('Список задач: ', list_of_task_lists[cur_list].name, '. Доступные команды:', sep='')
    print('1) Добавить задачу ', '2) Изменить задачу ', '3) Отметить задачу выполненной', '4) Удалить задачу')
    print('5) Посмотреть текущий список задач', '6) Посмотреть выполненные задачи', '7) Выйти из программы')
    print('8) Показать списки задач', '9) Посмотреть информацию о задаче')
    try:
        choice = int(input('\nЧто вы хотите сделать? Введите цифру: '))
    except ValueError:
        os.system("cls")
        print('Пожалуйста, введите число\n')
    else:
        print()
        if choice != 7:
            os.system("cls")
        if choice < 1 or choice > 9:
            os.system("cls")
            print('Пожалуйста, выберите одну команду из представленных\n')
        elif choice == 1:
            choice_1(list_of_task_lists, cur_list)
        elif choice == 2:
            choice_2(list_of_task_lists, cur_list)
        elif choice == 3:
            choice_3(list_of_task_lists, cur_list, user_name)
        elif choice == 4:
            choice_4(list_of_task_lists, cur_list)
        elif choice == 5:
            choice_5(list_of_task_lists, cur_list)
        elif choice == 6:
            choice_6(list_of_task_lists, cur_list)
        elif choice == 7:
            print('До скорой встречи!')
            return False, list_of_task_lists, user_name, count_of_lists, cur_list
        elif choice == 8:
            list_of_task_lists, count_of_lists, cur_list = \
                choice_8(list_of_task_lists, count_of_lists, cur_list)
        elif choice == 9:
            choice_9(list_of_task_lists, cur_list)
    return True, list_of_task_lists, user_name, count_of_lists, cur_list
