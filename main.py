import os
import hashlib
from classes import List
from functions import print_list, try_except, except_text, begin
from file_functions import write_data_into_file, read_data_from_file


def main():
    """
    Start point of program

    :return: nothing
    """
    os.system("cls")
    print('Добро пожаловать в консольный TO-DO list!')
    user_name = input('Введите ваше имя: ')
    try:
        f = open(f'{user_name}.txt', 'r', encoding='utf-8')
    except FileNotFoundError:  # значит, это новый пользователь
        f = open(f'{user_name}.txt', 'w', encoding='utf-8')
        password = input('Придумайте пароль: ')
        password = hashlib.sha1(password.encode()).hexdigest()  # хэширование пароля
        list_of_task_lists = []  # список списков задач
        print('\nСоздайте новый список задач!')
        list_of_task_lists.append(List('', [], []))
        list_of_task_lists[-1].name = input('Введите его название: ')
        print()
        cur_list = 0  # индекс текущего списка задач
        count_of_lists = 1  # количество списков задач
        flag = True
        while flag:
            flag, list_of_task_lists, user_name, count_of_lists, cur_list = \
                begin(list_of_task_lists, user_name, count_of_lists, cur_list)
        # запись полученных данных в файл
        f = write_data_into_file(f, password, count_of_lists, list_of_task_lists)
        f.close()
    else:
        # проверяем пароль
        file_password = f.readline().strip()
        password = input('Введите пароль: ')
        password = hashlib.sha1(password.encode()).hexdigest()
        password_is_correct = False
        while not password_is_correct:
            if password != file_password:
                password = input('Неверный пароль, попробуйте снова: ')
                password = hashlib.sha1(password.encode()).hexdigest()
            else:
                os.system("cls")
                password_is_correct = True
                # считываем данные из файла
                count_of_lists, list_of_task_lists = read_data_from_file(f)
                f.close()
                if count_of_lists > 0:
                    print_list(list_of_task_lists, 'списков ')
                    cur_list = input('Выберите, с каким хотите работать: ')
                    text1, text2 = except_text('список')
                    cur_list = try_except(cur_list, len(list_of_task_lists), text1, text2)
                else:  # значит еще не создано ни одного списка
                    print('Создайте новый список задач!')
                    list_of_task_lists.append(List('', [], []))
                    list_of_task_lists[-1].name = input('Введите его название: ')
                    cur_list = 0
                    count_of_lists += 1
                os.system("cls")
                flag = True
                while flag:
                    flag, list_of_task_lists, user_name, count_of_lists, cur_list =\
                        begin(list_of_task_lists, user_name, count_of_lists, cur_list)
                # запись полученных изменений обратно в файл
                f = open(f'{user_name}.txt', 'w', encoding='utf-8')
                f = write_data_into_file(f, password, count_of_lists, list_of_task_lists)
                f.close()


if __name__ == '__main__':
    main()
