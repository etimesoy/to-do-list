class List:

    def __init__(self, name, tasks, done_tasks):
        """
        Includes information about task list

        :param name: name of this task list
        :param tasks: list of tasks in this task list
        :param done_tasks: list of complete tasks in this task list
        """
        self.name = name
        self.tasks = tasks
        self.done_tasks = done_tasks

    def read_tasks(self, task_list):
        class_task_list = []
        for i in range(len(task_list)):
            class_task_list.append(Task())
            task_list[i] = task_list[i].split(', ')
            class_task_list[i].name = task_list[i][0]
            class_task_list[i].expiration_date = task_list[i][1]
            class_task_list[i].reminder = task_list[i][2]
            class_task_list[i].priority = int(task_list[i][3])
            class_task_list[i].description = task_list[i][4]
            self.tasks.append(class_task_list[i])

    def read_done_tasks(self, done_task_list):
        class_done_task_list = []
        for i in range(len(done_task_list)):
            class_done_task_list.append(Task())
            done_task_list[i] = done_task_list[i].split(', ')
            class_done_task_list[i].name = done_task_list[i][0]
            class_done_task_list[i].expiration_date = done_task_list[i][1]
            class_done_task_list[i].reminder = done_task_list[i][2]
            class_done_task_list[i].priority = int(done_task_list[i][3])
            class_done_task_list[i].description = done_task_list[i][4]
            self.done_tasks.append(class_done_task_list[i])


class Task:

    def __init__(self, name='', expiration_date='', reminder='', priority=3, description='0'):
        """
        Includes information about task

        :param name: the task itself
        :param expiration_date: deadline
        :param reminder: date and time of the reminder
        :param priority: 1 - high, 2 - medium, 3 - low
        :param description: some additional information
        """
        self.name = name
        self.expiration_date = expiration_date
        self.reminder = reminder
        self.priority = priority
        self.description = description

    def new(self):
        self.name = input('Введите задачу: ')
        self.expiration_date = input('Добавьте дату (дедлайн): ')
        self.reminder = input('Добавьте дату и время напоминания: ')
        temp = input('Добавьте приоритет (1-3): ')
        self.priority = add_priority(temp)
        self.description = input('Добавьте описание: ')
        if self.description == '':
            self.description = '0'

    def show(self):
        print('Задача:', self.name)
        print('Дата (дедлайн):', self.expiration_date)
        print('Напоминание (дата и время):', self.reminder)
        print('Приоритет:', self.priority)
        if self.description != '0':
            print('Описание:', self.description)
        else:
            print('Описание:', '')

    def change(self):
        print('(нажмите enter, если не хотите изменять какой-либо параметр)')
        new_name = input('Введите новое название задачи: ')
        if new_name != '':
            self.name = new_name
        new_expiration_date = input('Введите новую дату (дедлайн): ')
        if new_expiration_date != '':
            self.expiration_date = new_expiration_date
        new_reminder = input('Добавьте новые дату и время напоминания: ')
        if new_reminder != '':
            self.reminder = new_reminder
        new_priority = input('Введите новый приоритет задачи: ')
        if new_priority != '':
            self.priority = add_priority(new_priority)
        new_description = input('Введите новое описание: ')
        if new_description != '':
            self.description = new_description

from functions import add_priority
